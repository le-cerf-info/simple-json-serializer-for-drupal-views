<?php
namespace Drupal\simple_json_view_serializer\Plugin\views\style;

use Drupal\Core\Entity\EntityInterface;
use Drupal\rest\Plugin\views\style\Serializer;
use Drupal\views\Plugin\views\style\StylePluginBase;
use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;

/**
 * Simple JSON serializer style plugin.
 *
 * @ViewsStyle(
 *   id = "simple_json_serializer",
 *   title = @Translation("Simple JSON Serializer"),
 *   help = @Translation("Serializes views results in a simplified JSON format."),
 *   theme = "views_view_simple_json_serializer",
 *   display_types = {"data"}
 * )
 */
class SimpleJsonSerializer extends Serializer {

  /**
   * Handles the output for a Paragraph entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The Paragraph entity.
   * @param array $values
   *   The values array to append to.
   */
  private function handleParagraphOutput(EntityInterface $entity, array &$values): void {
    // For paragraphs, get all fields.
    $paragraph_values = [];

    foreach ($entity->getFields() as $paragraph_field_name => $paragraph_field) {
      if (!$paragraph_field->getFieldDefinition()->isComputed()) {
        $paragraph_values[$paragraph_field_name] = $paragraph_field->getValue();
      }
    }

    $values[] = $paragraph_values;
  }

  /**
   * Handles the output for a Taxonomy Term or other entity types.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   * @param array $values
   *   The values array to append to.
   */
  private function handleTaxonomyOutput(EntityInterface $entity, array &$values): void {
    // For others like taxonomy, retrieve label only.
    $values[] = $entity->label();
  }


  /**
   * {@inheritdoc}
   */
  public function render(): string
  {
    try {
      $output = [];

      foreach ($this->view->result as $row) {
        $row_output = [];

        foreach ($row->_entity->getFields() as $field_name => $field) {
          $field_type = $field->getFieldDefinition()->getType();

          switch ($field_type) {

            case 'entity_reference_revisions':
            case 'entity_reference':
              $values = [];

              foreach ($field as $item) {
                if ($item instanceof EntityReferenceItem) {
                  $entity = $item?->get('entity')?->getTarget()?->getValue() ?? NULL;

                  if (!empty($entity)) {
                    if ($entity->getEntityTypeId() == 'paragraph') {
                      $this->handleParagraphOutput($entity, $values);
                    }
                    else {
                      $this->handleTaxonomyOutput($entity, $values);
                    }
                  }
                }
              }

              $row_output[$field_name] = $values;
              break;

            default:
              $row_output[$field_name] = $field->getString();
              break;
          }
        }

        $output[] = $row_output;
      }

      return \json_encode(
        $output,
        JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_THROW_ON_ERROR
      );
    } catch (\Exception $e) {
      return \json_encode(
        [
          'message' => 'An error occurred :' . $e->getMessage(),
          'trace' => $e->getTraceAsString(),
        ],
        JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE
      );
    }

  }

  /**
   * {@inheritdoc}
   */
  public function getFormats(): array
  {
    return ['json'];
  }

}
