# Simple JSON Serializer for Drupal Views

## Description
`simple_json_serializer` is a style plugin for Drupal Views that serializes view results into a simplified JSON format. This serializer is tailored specifically to handle Paragraph and Taxonomy entities, offering custom output for each type.

## Features
- **Paragraph Serialization**: For each paragraph field in the view, the serializer provides all inner fields of the paragraph instead of just returning the paragraph's ID.
- **Taxonomy Serialization**: For taxonomy fields, the serializer returns only the taxonomy term's label.

## Installation
1. Require the module using Composer:
```bash
composer require tserres/simple_json_view_serializer
```

2. Enable the module:
  - Via the Drupal admin interface
  - Or using Drush:
```bash
drush en simple_json_view_serializer
```

## Usage
1. Create or edit a view.
2. Add or modify a "REST export" display.
3. In the format section, choose "Simple JSON Serializer".
4. Configure your view as required.
5. When accessing the display's REST URL, results will be serialized in the simplified JSON format defined by this plugin.

## License
This module is shared under the [GPLv2 license](http://www.gnu.org/licenses/gpl-2.0.html), consistent with Drupal.

## Support and Contributions
For issues or suggestions, open a ticket in our issue tracking system. Pull request contributions are also appreciated.

## Author
Thomas SERRES
